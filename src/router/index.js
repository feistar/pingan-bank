import Vue from 'vue'
import Router from 'vue-router'
import Main from '../views/Main'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'indexHome',
      component: Main
    },
    {
      path: '/home',
      name: 'main',
      component: Main
    },
  ]
})
